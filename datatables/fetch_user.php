<?php
//遍历所有员工信息
require_once('../config.php');
extract($_GET);
$totalCount = $link->query("SELECT * FROM `employees` ")->num_rows;
$search_where = "";
if(!empty($search)){
    $search_where = " where ";
    $search_where .= " name LIKE '%{$search['value']}%' ";
    $search_where .= " OR address LIKE '%{$search['value']}%'";
	$search_where .= " OR gender LIKE '%{$search['value']}%' ";
	$search_where .= " OR salary LIKE '%{$search['value']}%' ";
}
$columns_arr = array("id","name","address","salary","gender");
$query = "SELECT * FROM `employees` {$search_where}";
if(isset($order[0]['dir']))
{
	$query .= " ORDER BY {$columns_arr[$order[0]['column']]} {$order[0]['dir']}";
}
else
{
	$query .= " ORDER BY id DESC";
}
if($length != -1)
{
	$query .= ' LIMIT ' . $start . ', ' . $length;
}
$query = $link->query($query);
$recordsFilterCount = $link->query("SELECT * FROM `employees` {$search_where} ")->num_rows;



$recordsTotal= $totalCount;
$recordsFiltered= $recordsFilterCount;
$data = array();
while($row = $query->fetch_assoc()){
	$sub_array = array();
	$sub_array[] = $row['name'];
	$sub_array[] = $row['address'];
	$sub_array[] = $row['salary'];
	$sub_array[] = $row['gender'];
	$sub_array[] = '<button type="button" data-id="'.$row["id"].'" class="btn btn-warning btn-sm btn-edit">Edit</button>&nbsp;<button type="button" class="btn btn-danger btn-sm btn-delete" data-id="'.$row["id"].'">Delete</button>';
	$data[] = $sub_array;
}

echo json_encode(array('draw'=>$draw,'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered,'data'=>$data));

?>

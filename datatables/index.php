<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PHP Datatables ajax CRUD Example,Datatables增删改查例子</title>

    <!-- DataTables CSS library -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"/>
    <link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.js"></script>
    <!-- DataTables JS library -->

    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

    <style>
        .bs-example {
            margin: 20px;
        }
    </style>
</head>
<body>
<div class="bs-example">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header clearfix">
                    <h2 class="float-left">员工列表</h2>
                    <h2 class="float-left">此代码原至于<a href="https://xpertphp.com/php-datatables-crud-example/"> Xpertphp.com</a> 我只是对其进行了汉化</h2>
                    <a href="javascript:void(0)" class="btn btn-primary float-right add-model mb-2"> 添加员工 </a>
                </div>

                <table id="usersListTable" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>姓名</th>
                        <th>地址</th>
                        <th>工资</th>
                        <th>性别</th>
                        <th>上操作</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>姓名</th>
                        <th>地址</th>
                        <th>工资</th>
                        <th>性别</th>
                        <th>上操作</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
</body>

<div class="modal fade" id="edit-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="userCrudModal">更新员工信息</h4>
            </div>
            <div class="modal-body">
                <form id="update-form" name="update-form" class="form-horizontal">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" class="form-control" id="mode" name="mode" value="update">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name"
                                   value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">address</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="address" name="address" placeholder="Enter address"
                                   value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="salary" class="col-sm-2 control-label">salary</label>
                        <div class="col-sm-12">
                            <input type="number" class="form-control" id="salary" name="salary" placeholder="Enter salary"
                                   value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gender" class="col-sm-2 control-label">gender</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="gender" name="gender" placeholder="Enter gender"
                                   value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary mt-2" id="btn-save" value="create">Update</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="userCrudModal">添加员工</h4>
            </div>
            <div class="modal-body">
                <form id="add-form" name="add-form" class="form-horizontal">
                    <input type="hidden" class="form-control" id="mode" name="mode" value="add">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name"
                                   value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">address</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="address" name="address" placeholder="Enter address"
                                   value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="salary" class="col-sm-2 control-label">salary</label>
                        <div class="col-sm-12">
                            <input type="number" class="form-control" id="salary" name="salary" placeholder="Enter salary"
                                   value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gender" class="col-sm-2 control-label">gender</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="gender" name="gender" placeholder="Enter gender"
                                   value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary mt-2" id="btn-save" value="create">添加</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#usersListTable').DataTable({
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.6/i18n/zh.json',
            },
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": "fetch_user.php"
        });
    });


    /*  add user model */
    $('.add-model').click(function () {
        $('#add-modal').modal('show');
    });

    // add form submit
    $('#add-form').submit(function (e) {

        e.preventDefault();

        // ajax
        $.ajax({
            url: "insert_user.php",
            type: "POST",
            data: $(this).serialize(), // get all form field value in serialize form
            success: function () {
                var oTable = $('#usersListTable').dataTable();
                oTable.fnDraw(false);
                $('#add-modal').modal('hide');
                $('#add-form').trigger("reset");
            }
        });
    });

    /* edit user function */
    $('body').on('click', '.btn-edit', function () {
        var id = $(this).data('id');
        $.ajax({
            url: "single_user.php",
            type: "POST",
            data: {id: id},
            dataType: 'json',
            success: function (result) {
                $('#id').val(result.data.id);
                $('#name').val(result.data.name);
                $('#address').val(result.data.address);
                $('#salary').val(result.data.salary);
                $('#gender').val(result.data.gender);
                $('#edit-modal').modal('show');
            }
        });
    });

    // add form submit
    $('#update-form').submit(function (e) {

        e.preventDefault();

        // ajax
        $.ajax({
            url: "update_user.php",
            type: "POST",
            data: $(this).serialize(), // get all form field value in serialize form
            success: function () {
                var oTable = $('#usersListTable').dataTable();
                oTable.fnDraw(false);
                $('#edit-modal').modal('hide');
                $('#update-form').trigger("reset");
            }
        });
    });

    /* DELETE FUNCTION */
    $('body').on('click', '.btn-delete', function () {
        var id = $(this).data('id');
        if (confirm("Are You sure want to delete !")) {
            $.ajax({
                url: "delete_user.php",
                type: "POST",
                data: {id: id},
                dataType: 'json',
                success: function (result) {
                    var oTable = $('#usersListTable').dataTable();
                    oTable.fnDraw(false);
                }
            });
        }
        return false;
    });

</script>
</html>
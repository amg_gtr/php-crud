<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employees Details Manage</title>
    <link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcdn.net/ajax/libs/font-awesome/6.4.0/css/all.min.css" rel="stylesheet">

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.4/jquery.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/popper.js/2.11.7/cjs/popper.min.js"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/js/bootstrap.min.js"></script>
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
        table tr td:last-child{
            width: 120px;
        }
    </style>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</head>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="mt-5 mb-3 clearfix">
                    <h2 class="pull-left">Employees Details</h2>
                    <a href="create.php" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New Employee</a>
                </div>
                <?php
                // Include config file
                require_once "config.php";
                $results_per_page = 15;
                // 构造SQL查询语句
                $sql = "SELECT COUNT(*) AS total FROM employees";

                $result = $link->query($sql);
                $row = $result->fetch_assoc();
                $total_results = $row['total'];
                $total_pages = ceil($total_results / $results_per_page);

                // 获取当前页码
                $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
                $start_index = ($current_page - 1) * $results_per_page;

                // Attempt select query execution
                $sql = "SELECT * FROM employees order by  id desc LIMIT $start_index, $results_per_page ";
                if($result = mysqli_query($link, $sql)){
                    if(mysqli_num_rows($result) > 0){
                        echo '<table class="table table-bordered table-striped">';
                        echo "<thead>";
                        echo "<tr>";
                        echo "<th>#</th>";
                        echo "<th>Name</th>";
                        echo "<th>Address</th>";
                        echo "<th>Salary</th>";
                        echo "<th>Action</th>";
                        echo "</tr>";
                        echo "</thead>";
                        echo "<tbody>";
                        while($row = mysqli_fetch_array($result)){
                            echo "<tr>";
                            echo "<td>" . $row['id'] . "</td>";
                            echo "<td>" . $row['name'] . "</td>";
                            echo "<td>" . $row['address'] . "</td>";
                            echo "<td>" . $row['salary'] . "</td>";
                            echo "<td>";
                            echo '<a href="read.php?id='. $row['id'] .'" class="mr-3" title="View Record" data-toggle="tooltip"><span class="fa fa-eye"></span></a>';
                            echo '<a href="update.php?id='. $row['id'] .'" class="mr-3" title="Update Record" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>';
                            echo '<a href="delete.php?id='. $row['id'] .'" title="Delete Record" data-toggle="tooltip"><span class="fa fa-trash"></span></a>';
                            echo "</td>";
                            echo "</tr>";
                        }
                        echo "</tbody>";
                        echo "</table>";
                        // Free result set
                        mysqli_free_result($result);
                    } else{
                        echo '<div class="alert alert-danger"><em>No records were found.</em></div>';
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }

                // Close connection
                mysqli_close($link);
                ?>
            </div>
            <nav aria-label="Page navigation">
                <ul class="pagination">
            <?php
            // 设置要显示的页码范围
            $range = 3;

            // 计算起始页码和结束页码
            $start = $current_page - $range;
            $end = $current_page + $range;

            // 修正起始页码和结束页码的边界
            if ($start < 1) {
                $start = 1;
                $end = min($total_pages, $start + 2 * $range);
            }
            if ($end > $total_pages) {
                $end = $total_pages;
                $start = max(1, $end - 2 * $range);
            }

            // 生成分页链接
            for ($page = $start; $page <= $end; $page++) {
                echo '<li class="page-item';
                if ($page == $current_page) {
                    echo ' active';
                }
                echo '"><a class="page-link" href="?page=' . $page . '">' . $page . '</a></li>';
            }
            ?></ul></nav>
        </div>
    </div>
</div>
</body>
</html>
<?php
/*备份数据库功能
PHP需要启用 exec
*/

// MySQL数据库连接信息
$servername = "localhost"; // 数据库服务器名称
$username = "username"; // 数据库用户名
$password = "password"; // 数据库密码
$dbname = "dbname"; // 数据库名称
$conn = new mysqli($servername, $username, $password, $dbname);

// 检查连接
if ($conn->connect_error) {
  die("连接失败: " . $conn->connect_error);
}

// 导出数据库
if(isset($_POST['backup'])){
  $backup_file = 'backup-' . date("Y-m-d-H-i-s") . '.sql';
  $command = "mysqldump --user=$username --password=$password --host=$servername $dbname > $backup_file";
  exec($command);
   $message = "数据库已备份到文件：" . $backup_file;
}
?>

<!DOCTYPE html>
<html>
<body>
<div>
    <?php
    //如果isset($message)存在，则输出$message
    // 检查$message是否被设置
    if (isset($message)) {
    echo $message;
    unset($message);
    }
    // 输出$message
    ?></div>
<form method="post">
  <input type="submit" name="backup" value="备份数据库">
</form>
</body>
</html>
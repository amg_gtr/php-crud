DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100)  NOT NULL,
  `address` varchar(255)  NOT NULL,
  `salary` int(10) NOT NULL,
  `gender` varchar(10),
  PRIMARY KEY (`id`) 
)
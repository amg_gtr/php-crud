<?php
include 'config.php';

// Define how many results you want per page
$results_per_page = 12;

// Determine which page number to load
$page = $_GET['page'];

// Calculate the starting and ending item indices for the current page
$start_index = ($page - 1) * $results_per_page;
$end_index = $start_index + $results_per_page;

// Fetch content from database based on category (if provided)
//三元判断 获取category_id
//$category_id = isset($_GET['$category_id']) ? $_GET['$category_id'] :null;
$gender = isset($_GET['gender']) ? $_GET['gender'] : null;

if($gender !== null) {
    $sql = "SELECT * FROM employees WHERE gender = $gender LIMIT $start_index, $results_per_page";
} else {
    $sql = "SELECT * FROM employees LIMIT $start_index, $results_per_page";
}

$result = mysqli_query($link, $sql);

if($result){
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)){
            echo '<div class="col-lg-2 col-md-3 p-2 border">';
            echo '<div class="thumbnail">';
            //echo '<img class="img-fluid" src="data:image/jpeg;base64,' . base64_encode($row['index_Image']) . '" alt="Product Image">';
            echo '<div class="caption">';
            echo '<h4><a href="read.php?id=' . $row['id'] . '">'  . $row['name'] .'--'. $row['gender'] .'</a></h4>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
    } else{
        echo '<div class="col-md-12">No more content found</div>';
    }
} else{
    echo '<div class="col-md-12">Error fetching content'.$gender.'</div>';
}

// Close connection
mysqli_close($link);
?>
<!-- 
页面载入的时候加载24个信息；
如果没有选择性别，加载全部，选择性别了，加载指定性别；
通过load_more_12-ajax.php加载更多来加载额外的12个；
2023/09/25-->
<!doctype html>
<html lang="zh" data-bs-theme="auto">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>手动加载_素材分类</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/5.2.3/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <div class="row">
         <div class="col-md-3">
            <h2>素材分类</h2>
            <ul class="list-group">
                <?php
                include 'config.php';
                // Fetch categories from database
                $sql = "SELECT DISTINCT gender FROM employees";
                $result = mysqli_query($link, $sql);

                if ($result) {
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<li class="list-group-item"><a href="?gender=' . $row['gender'] . '">' . $row['gender'] . '</a></li>';
                        }
                    } else {
                        echo '<li class="list-group-item">No gender found</li>';
                    }
                } else {
                    echo '<li class="list-group-item">Error fetching gender</li>';
                }
                ?>
            </ul>
        </div>
        <div class="col-md-9">
            <h2>员工列表</h2>
            <div class="row" id="contentList">
                <?php
                // Fetch content from database
                $gender = isset($_GET['gender']) ? $_GET['gender'] : null;

                if ($gender !== null) {
                    $sql = "SELECT * FROM employees WHERE gender = $gender LIMIT 24 ";
                } else {
                    $sql = "SELECT * FROM employees LIMIT 24";
                }
                //$sql = "SELECT * FROM 1_contents LIMIT 24";
                $result = mysqli_query($link, $sql);

                if ($result) {
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<div class="col-lg-2 col-md-3 p-2 border">';
                            echo '<div class="thumbnail ">';
                         //echo '<img class="img-fluid" src="data:image/jpeg;base64,' . base64_encode($row['index_Image']) . '" alt="Product Image">';
                            echo '<div class="caption">';
                            echo '<h4><a href="read.php?id=' . $row['id'] . '">'  . $row['name'] .'--'. $row['gender'] .'</a></h4>';
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                        }
                    } else {
                        echo '<div class="col-md-12">No content found</div>';
                    }
                } else {
                    echo '<div class="col-md-12">Error fetching content</div>';
                }
                ?>
            </div>
            <button class="btn btn-primary mt-3 loadMore">加载更多</button>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
    $(document).ready(function () {
        var page = 3; // Starting page for loading more content

        $('.loadMore').click(function () {
            var gender = <?php echo isset($_GET['gender']) ? $_GET['gender'] : 'null'; ?>;
            var url = 'load_more_12_ajax.php?page=' + page;

            if (gender !== null) {
                url += '&gender=' + gender;
            }

            $.ajax({
                url: url,
                type: 'get',
                dataType: 'html',
                success: function (response) {
                    $('#contentList').append(response);
                    page++;
                },
                error: function () {
                    alert('Error loading content');
                }
            });
        });
    });
</script>

<script src="https://getbootstrap.com/docs/5.3/dist/js/bootstrap.bundle.min.js" ></script>

</body>
</html>